/*
   Nama     : Mohamad Ardi
   Kelas    : IoT-4-D
   Project  : DHT_11
   Date     : 02 Mei 2021
*/
#include "DHT.h"
#define DHTPIN 5
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("DHT11 Sensor");
  dht.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
  float h = dht.readHumidity();
  float t = dht.readTemperature();

  if (isnan(h) || isnan(t)) {
    Serial.println("Failed to read from DHT Sensor");
    return;
    }

    Serial.print("Humidity = ");
    Serial.print(h);
    Serial.print("%\t");
    Serial.print("Temperature = ");
    Serial.print(t);
    Serial.println("°C ");
    delay(2000);
}
